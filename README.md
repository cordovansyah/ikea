# IKEA

IKEA is an ARKit technology-based iOS app that provides an immersive experience in simulating pre-purchased furniture items, ensuring quality assurances prior buying one through augmented reality world

# Motivation
I love to learn and build stuffs, and currently we're entering the AI First world where everything shall be gradually automated, including
the way we think and build apps for both mobile and web. I am keen to learn from widely preserved references about harnessing the power of
Artificial Intelligence in building a useful app for anyone to learn and adapt to the technological growth, and for sure, looking forward to
be part of a greater team for behemothal contribution towards global society, through technology. 

## Why I Build This App?
Curiosity. I watched WWDC 18 and learn several things regarding several Machine Learning libraries and seeing the Apple team's demonstration
of how easy it is to incorporate ML model to iOS app where non-ML engineers can focus in delivering better user experience, or so called intelligent
user experience made myself, flabbergasted. I am currently work as a UX Engineer, and my curiosity comes out of blue about how ML could work in unison
alongisde UX and finally I got the answer, and decided to focus on building more AI related app prototypes, specifically enhancing it's user experience


### Motivation

### Prerequisites
```
Xcode 10.0
Swift 4.0 / 4.2 
iOS 12
```

## Built With

* [MNIST Digit Classifier](http://yann.lecun.com/exdb/mnist/) - CoreML Model Used
* [CoreML Model Libraries](https://coreml.store/) - CoreML Model Used


## Acknowledgments
* Amazing CoreML Model Libraries !! (https://coreml.store/)

